import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MediaObserver } from '@angular/flex-layout';

import { AuthenticationService, CredentialsService } from '@app/auth';
import { NoxbotService } from '@shared/services/noxbot.service';
import { ManagedPageModel } from '@shared/services/managedpage.model';

import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { NotificationService } from '@app/@shared/services/notification.service';
import { CoreModule, Logger } from '@core';
import { MatDialog } from '@angular/material/dialog';
import { DialogResult, DialogComponent, DialogData } from '@app/@shared/dialog/dialog.component';
import { FbPermission } from '@app/@shared/services/facebook-graph-api.service';
import { environment } from '@env/environment';
import { ManagedPageService } from '@app/@shared/services/managed-page.service';
import { ErrorCodes, ErrorResponseData } from '@app/@shared/services/error-response.model';

const log = new Logger('ShellComponent');
const isBotActivatedKey = 'isBotActivated';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss'],
})
export class ShellComponent implements OnInit {
  isBotActivating = false;
  isChecked = false;

  facebookPermissions: FbPermission[] = [];

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private credentialsService: CredentialsService,
    private noxbotService: NoxbotService,
    private titleService: Title,
    private media: MediaObserver,
    private notificationService: NotificationService,
    private coreModule: CoreModule,
    public managedPageService: ManagedPageService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    // Do a version check and log out in case

    this.managedPageService.getListOfPages().subscribe(
      (pages) => {
        log.debug(`managedPageService.getListOfPages().next()`);
        if (pages.length > 0) {
          // this.getAndCheckPermissions();
        } else {
          this.alertNoPages();
        }
      },
      (error) => {
        log.debug(`managedPageService.getListOfPages().error()`, error);
        let requestError: ErrorResponseData = error.data;
        this.alertFailedToGetPages(requestError);
      }
    );
  }

  get version() {
    return environment.version;
  }

  get localStorageBytesUsed(): number {
    var localStorageUsed = 0,
      dataLength,
      item;
    for (item in localStorage) {
      if (!localStorage.hasOwnProperty(item)) {
        continue;
      }
      dataLength = (localStorage[item].length + item.length) * 2;
      localStorageUsed += dataLength;
    }
    return localStorageUsed;
  }

  get localStorageBytesAvailable(): number {
    return 5 * 1024 * 1024;
  }

  get localStorageUsed() {
    return (this.localStorageBytesUsed / 1024 / 1024).toFixed(2);
  }

  get localStorageAvailable() {
    return (this.localStorageBytesAvailable / 1024 / 1024).toFixed(2);
  }

  get localStorageUsedPercentage(): number {
    return (this.localStorageBytesUsed / this.localStorageBytesAvailable) * 100;
  }

  get pages() {
    return this.managedPageService.managedPages;
  }

  getAndCheckPermissions() {
    const userId = this.credentialsService.credentials.id;
    const acessToken = this.activePage.accessToken;
    this.noxbotService.getFacebookPermissions(userId, acessToken).subscribe(
      (permissions) => {
        log.debug(`Permissions:`);
        log.debug(permissions);

        this.facebookPermissions = permissions;

        const requiredPermissions: string[] = environment.scope.split(',');

        let missingPermissions: string[] = [];

        requiredPermissions.forEach((perm) => {
          if (!this.checkPermission(perm)) {
            missingPermissions.push(perm);
          }
        });

        if (missingPermissions.length > 0) {
          this.alertMissingPermissions(missingPermissions);
        }
      },
      (error) => {
        // TODO: What to do if we failed to get the list of permissions?
      }
    );
  }

  checkPermission(permission: string): boolean {
    return (
      this.facebookPermissions.findIndex((perm) => {
        return perm.permission === permission && perm.status === 'granted';
      }) > -1
    );
  }

  alertMissingPermissions(missingPermissions: string[]) {
    let missingPermissionsList = '';

    if (missingPermissions.length > 0) {
      missingPermissionsList = `\n\nMissing permissions:\n${missingPermissions.join('\n')}`;
    }

    let dialogData = new DialogData();
    dialogData.positiveActionText = 'OK';
    dialogData.message = `Nox Bot was not granted the permissions it needs to function properly. Please relogin and grant the Facebook permissions requested.${missingPermissionsList}`;
    let dialogRef = this.dialog.open(DialogComponent, { data: dialogData });

    dialogRef.afterClosed().subscribe((result: DialogResult) => {
      this.logout();
    });
  }

  alertNoPages() {
    let dialogData = new DialogData();
    dialogData.positiveActionText = 'OK';
    dialogData.message = `Your account does not have any pages it can moderate. Please try using a different account.`;
    let dialogRef = this.dialog.open(DialogComponent, { data: dialogData });

    dialogRef.afterClosed().subscribe((result: DialogResult) => {
      this.logout();
    });
  }

  alertFailedToGetPages(errorData: ErrorResponseData) {
    if (errorData.code) {
      // There's an error data code which means it's our own error data
      if (errorData.code == ErrorCodes.FacebookError) {
        // It's a Facebook error
        this.alertFacebookError(errorData);
      } else {
        // Other error
        this.alertOtherError(errorData);
      }
    } else {
      // No error data
      this.alertGenericError();
    }
  }

  alertFacebookError(errorData: ErrorResponseData) {
    let dialogData = new DialogData();
    dialogData.positiveActionText = 'OK';
    dialogData.message = `Could not get linked pages. Please relogin and check the Facebook permissions you granted to Nox Bot and review the pages you linked.
    
    Facebook error details:
    ${JSON.stringify(errorData.facebookError, null, ' ')}`;
    let dialogRef = this.dialog.open(DialogComponent, { data: dialogData });

    dialogRef.afterClosed().subscribe((result: DialogResult) => {
      this.logout();
    });
  }

  alertOtherError(errorData: ErrorResponseData) {
    let dialogData = new DialogData();
    dialogData.positiveActionText = 'Retry';
    dialogData.negativeActionText = 'Log out';
    dialogData.message = `${errorData.message}\n\nCode: ${errorData.code}`;
    let dialogRef = this.dialog.open(DialogComponent, { data: dialogData });

    dialogRef.afterClosed().subscribe((result: DialogResult) => {
      if (result == DialogResult.POSITIVE) {
        location.reload();
      } else if (result == DialogResult.NEGATIVE) {
        this.logout();
      }
    });
  }

  alertGenericError() {
    let dialogData = new DialogData();
    dialogData.positiveActionText = 'Retry';
    dialogData.negativeActionText = 'Log out';
    dialogData.message = `An unexpected error has occurred. Please report this to the developers to help find out the cause of the problem and include detailed steps you performed until you encountered this error.
      
      Apologies for this and thank you for bearing with us as we try to improve Nox Bot.`;
    let dialogRef = this.dialog.open(DialogComponent, { data: dialogData });

    dialogRef.afterClosed().subscribe((result: DialogResult) => {
      if (result == DialogResult.POSITIVE) {
        location.reload();
      } else if (result == DialogResult.NEGATIVE) {
        this.logout();
      }
    });
  }

  alertCantSwitchPageWhenLive() {
    let dialogData = new DialogData();
    dialogData.positiveActionText = 'OK';
    dialogData.message = "You can't switch pages when Nox Bot is on";
    let dialogRef = this.dialog.open(DialogComponent, { data: dialogData });
  }

  alertNewVersion() {
    let dialogData = new DialogData();
    dialogData.positiveActionText = 'OK';
    dialogData.message = 'A newer version of Nox Bot is available. Please log in again.';
    let dialogRef = this.dialog.open(DialogComponent, { data: dialogData });

    dialogRef.afterClosed().subscribe((result: DialogResult) => {
      this.logout();
    });
  }

  logout() {
    this.authenticationService.logout().subscribe(() => this.router.navigate(['/login'], { replaceUrl: true }));
  }

  get username(): string | null {
    const credentials = this.credentialsService.credentials;
    return credentials ? credentials.first_name : null;
  }

  get activePage(): ManagedPageModel | null {
    const page = this.credentialsService.activePage;
    return page ? page : null;
  }

  isActivePage(page: ManagedPageModel) {
    if (this.activePage) {
      return page.id === this.activePage.id;
    }
    return false;
  }

  private setActivePage(page: ManagedPageModel) {
    this.credentialsService.setActivePage(page);
  }

  switchPage(page: ManagedPageModel) {
    if (page.id != this.activePage.id) {
      if (this.isChecked) {
        this.alertCantSwitchPageWhenLive();
      } else {
        this.setActivePage(page);
      }
    }
  }

  get isMobile(): boolean {
    return this.media.isActive('xs') || this.media.isActive('sm');
  }

  get title(): string {
    return this.titleService.getTitle();
  }
}
