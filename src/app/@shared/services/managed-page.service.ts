import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { NoxbotService } from './noxbot.service';
import { ManagedPageModel } from './managedpage.model';
import { Logger } from './../../@core';
import { CredentialsService } from '../../auth/credentials.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '@env/environment';
import { catchError, map, share } from 'rxjs/operators';

const log = new Logger('ManagedPageService');
const managedPagesKey = 'managedPages';

@Injectable({
  providedIn: 'root',
})
export class ManagedPageService {
  _userManagedPages: ManagedPageModel[] = [];
  _isFetchingManagedPages: boolean = false;
  _errorFetchingManagedPages: boolean = false;

  constructor(
    private noxbotService: NoxbotService,
    private credentialsService: CredentialsService,
    private http: HttpClient
  ) {
    const cachedPages = sessionStorage.getItem(managedPagesKey);

    if (cachedPages) {
      this._userManagedPages = JSON.parse(cachedPages);
      log.debug(`Loaded pages from cache`);
    } else {
      this.getListOfPages().subscribe((pages) => {
        log.debug(`Fetched pages from server`);
      });
    }
  }

  get managedPages(): ManagedPageModel[] | [] {
    return this._userManagedPages;
  }

  get isFetchingManagedPages(): boolean {
    return this._isFetchingManagedPages;
  }

  get errorFetchingManagedPages(): boolean {
    return this._errorFetchingManagedPages;
  }

  clearCache() {
    this._userManagedPages = [];
    sessionStorage.removeItem(managedPagesKey);
  }

  public getListOfPages(): Observable<ManagedPageModel[]> {
    this.clearCache();
    const obsPages = this.noxbotService.getListOfPages();
    this._isFetchingManagedPages = true;
    this._errorFetchingManagedPages = false;
    obsPages.subscribe(
      (pages) => {
        log.debug(`Get managed pages: `, pages);
        this._isFetchingManagedPages = false;
        if (pages.length > 0) {
          // Only cache if not empty
          this._userManagedPages = pages;
          sessionStorage.setItem(managedPagesKey, JSON.stringify(this._userManagedPages));

          let activePage = this.credentialsService.activePage;

          if (activePage) {
            // There's an active page, let's update the details if we have it
            let activePageId = this.credentialsService.activePage.id;
            let updatedPage = this._userManagedPages.find((p) => {
              return p.id == activePageId;
            });

            if (updatedPage) {
              // We have an updated data, so let's use that
              this.credentialsService.setActivePage(updatedPage);
              log.debug(`Updated active page data (${activePage.name})`, updatedPage);
            } else {
              // No updated data for the active page which means it might have been unlinked,
              // so just use the first page in the array
              this.credentialsService.setActivePage(this._userManagedPages[0]);
              log.debug(
                `No updated data for the active page; using the first page (${this._userManagedPages[0].name})`
              );
            }
          } else {
            // There's no active page yet, so let's just use the first page in the array
            this.credentialsService.setActivePage(this._userManagedPages[0]);
            log.debug(`No active page; using the first page (${this._userManagedPages[0].name})`);
          }
        }
      },
      (error) => {
        this._isFetchingManagedPages = false;
        this._errorFetchingManagedPages = true;
      }
    );
    return obsPages;
  }
}
