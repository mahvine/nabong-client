export class ManagedPageModel {
  public id: string = '';
  public name: string = '';
  public userId: string = '';
  public email: string = '';
  public accessToken: string = '';
  public hashToken: string = '';
  public hasTransitionedToNpe: boolean = false;
}
