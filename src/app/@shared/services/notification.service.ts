import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  constructor(public snackBar: MatSnackBar) {}

  openSnackBar(message: string, action: string, duration: number = 5000) {
    this.snackBar.open(message, action, {
      duration,
      verticalPosition: 'bottom',
      horizontalPosition: 'center',
    });
  }

  error(message: string, duration: number = 5000) {
    this.openSnackBar(message, 'Close', duration);
  }

  message(message: string, duration: number = 5000) {
    this.openSnackBar(message, 'Close', duration);
  }
}
