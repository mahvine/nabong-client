export class PagingResponse<T> {
  public data: T[];
  public limit: number;
  public skip: number;
  public total: number;
}

export class TransactionModel {
  public id?: number;
  public message?: string;
  public pageId?: string;
  public senderId?: string;
  public createdAt?: string;
  public metadata?: any;
  public transactionStatus?: string;
}
