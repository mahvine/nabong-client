import { CredentialsService } from '@app/auth';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, catchError, share, mergeMap } from 'rxjs/operators';
import { Observable, throwError, of } from 'rxjs';
import { NotificationService } from './notification.service';
import {
  FacebookGraphApiService,
  FbPost,
  FbComment,
  FbReaction,
  FbPermission,
  FbVideo,
} from './facebook-graph-api.service';
import { environment } from '@env/environment';
import { Logger } from '@app/@core';
import { VideoResponse } from '../facebook.model';
import { SocialUser } from '../social-user.model';
import { ManagedPageModel } from './managedpage.model';
import { from } from 'rxjs';
import { PagingResponse, TransactionModel } from './nabong.model';
const log = new Logger('NoxbotService');

/**
 * A services that communicates with noxbot REST API services
 */
@Injectable({
  providedIn: 'root',
})
export class NoxbotService {
  constructor(
    private http: HttpClient,
    private credentialsService: CredentialsService,
    private facebookGraphApiService: FacebookGraphApiService
  ) {}

  public getFullFacebookObjectId(objectId: string): string {
    let pageId = this.credentialsService.activePage.id;
    let fullObjectId = objectId.startsWith(pageId + '_') ? objectId : pageId + '_' + objectId;
    log.debug(`getFullFacebookObjectId(): ${fullObjectId}`);
    return fullObjectId;
  }

  public getFullFacebookObjectIdFromVideoPermalink(permalink: string): string {
    let pageId = this.credentialsService.activePage.id;
    let delimiter = '/videos/';
    let postId = permalink.substring(permalink.indexOf(delimiter) + delimiter.length).replace(/\//g, '');
    let fullObjectId = pageId + '_' + postId;
    log.debug(`getFullFacebookObjectIdFromVideoPermalink(): ${fullObjectId}`);
    return fullObjectId;
  }

  public getListOfPages(): Observable<ManagedPageModel[]> {
    const socialUser = this.credentialsService.credentials;
    return this.http
      .get<ManagedPageModel[]>(
        `${environment.serverUrl}/api/pages?fbId=${socialUser.id}&userEmail=${socialUser.email}`,
        {
          observe: 'response',
          headers: this.createHeaders(socialUser),
        }
      )
      .pipe(
        share(),
        map((response) => response.body),
        catchError((response) => {
          throw response.error;
        })
      );
  }

  public getListOfTransactions(): Observable<PagingResponse<TransactionModel>> {
    const pageModel = this.credentialsService.activePage;
    return this.http
      .get<PagingResponse<TransactionModel>>(
        `${environment.serverUrl}/api/transaction?pageId=${pageModel.id}&$sort[createdAt]=-1&$limit=1000`,
        {
          observe: 'response',
          headers: this.createHeadersPage(pageModel),
        }
      )
      .pipe(
        share(),
        map((response) => response.body),
        catchError((response) => {
          throw response.error;
        })
      );
  }

  public updateStatus(txn: TransactionModel): Observable<String> {
    const pageModel = this.credentialsService.activePage;
    return this.http
      .put<String>(`${environment.serverUrl}/api/transaction/${txn.id}`, txn, {
        observe: 'response',
        headers: this.createHeadersPage(pageModel),
      })
      .pipe(
        share(),
        map((response) => response.body),
        catchError((response) => {
          throw response.error;
        })
      );
  }

  getFacebookPermissions(pageId: string, pageAuthToken: string): Observable<FbPermission[]> {
    return this.facebookGraphApiService.getPermissions(pageId, pageAuthToken);
  }

  getLiveVideos(pageId: string, pageAuthToken: string, hasTransitionedToNpe: boolean): Observable<VideoResponse> {
    return this.facebookGraphApiService.getLiveVideos(pageId, pageAuthToken, hasTransitionedToNpe);
  }

  getPosts(pageId: string, pageAuthToken: string): Observable<FbPost[]> {
    return this.facebookGraphApiService.getPosts(pageId, pageAuthToken);
  }

  getManyPosts(pageId: string, pages: number, pageAuthToken: string): Observable<FbPost[]> {
    return from(this.facebookGraphApiService.getManyPosts(pageId, pages, pageAuthToken));
  }

  getReactions(fullObjectId: string, limit: number, pageAuthToken: string): Observable<FbReaction[]> {
    return this.facebookGraphApiService.getReactions(fullObjectId, limit, pageAuthToken);
  }

  getComments(fullObjectId: string, limit: number, pageAuthToken: string): Observable<FbComment[]> {
    return this.facebookGraphApiService.getComments(fullObjectId, limit, pageAuthToken);
  }

  getAllComments(fullObjectId: string, pageAuthToken: string): Observable<FbComment[]> {
    return from(this.facebookGraphApiService.getAllComments(fullObjectId, pageAuthToken));
  }

  getAllCommentsForPosts(fullObjectIds: string[], pageAuthToken: string): Observable<FbComment[][]> {
    return from(this.facebookGraphApiService.getAllCommentsForPosts(fullObjectIds, pageAuthToken));
  }

  replyToComment(comment: FbComment, pageAuthToken: string, message: string) {
    return this.facebookGraphApiService.replyToComment(comment, pageAuthToken, message);
  }

  postComment(pageId: string, hasTransitionedToNpe: boolean, video: FbVideo, pageAuthToken: string, message: string) {
    return this.facebookGraphApiService.postComment(pageId, hasTransitionedToNpe, video, pageAuthToken, message);
  }

  deleteComment(comment: FbComment, pageAuthToken: string) {
    return this.facebookGraphApiService.deleteComment(comment, pageAuthToken);
  }

  likeComment(comment: FbComment, pageAuthToken: string) {
    return this.facebookGraphApiService.likeComment(comment, pageAuthToken);
  }

  createHeaders(socialUser: SocialUser) {
    return { 'x-authtoken': socialUser.access_token, 'x-hashtoken': socialUser.hashToken };
  }

  createHeadersPage(managedPageModel: ManagedPageModel) {
    return { 'x-authtoken': managedPageModel.accessToken, 'x-hashtoken': managedPageModel.hashToken };
  }
}
