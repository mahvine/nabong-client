import { Injectable } from '@angular/core';
import { CredentialsService } from '@app/auth';
import { HttpClient } from '@angular/common/http';
import { map, catchError, share } from 'rxjs/operators';
import { Observable, throwError, of } from 'rxjs';
import { NotificationService } from './notification.service';
import { environment } from '@env/environment';
import { Logger } from '@app/@core';
import { Post, PostResponse, Video, VideoResponse } from '../facebook.model';
import { ManagedPageModel } from './managedpage.model';
const log = new Logger('FacebookGraphApiService');

@Injectable({
  providedIn: 'root',
})
export class FacebookGraphApiService {
  constructor(
    private http: HttpClient,
    private credentialsService: CredentialsService,
    private notificationService: NotificationService
  ) {}

  public getPermissions(userId: string, userAccessToken: string): Observable<FbPermission[]> {
    return this.http
      .get<FbResponse<FbPermission[]>>(
        `https://graph.facebook.com/v10.0/${userId}/permissions?access_token=${userAccessToken}`,
        { observe: 'response' }
      )
      .pipe(
        share(),
        map((response) => {
          let permissions = response.body.data;

          log.info(permissions);

          return permissions as FbPermission[];
        })
      );
  }

  // public getListOfPages(userId: string, userAccessToken: string): Observable<PageModel[]> {
  //   return this.http
  //     .get<FbResponse<FbPage[]>>(
  //       `https://graph.facebook.com/v10.0/${userId}/accounts?fields=access_token,name,id,tasks&access_token=${userAccessToken}`,
  //       { observe: 'response' }
  //     )
  //     .pipe(
  //       share(),
  //       map((response) => {
  //         let pages = response.body.data;

  //         log.info(pages);

  //         return pages as PageModel[];
  //       })
  //     );
  // }

  getLiveVideos(pageId: string, pageAuthToken: string, hasTransitionedToNpe: boolean): Observable<VideoResponse> {
    return this.http
      .get<VideoResponse>(
        `https://graph.facebook.com/v10.0/${pageId}/live_videos?access_token=${pageAuthToken}&fields=video,from,status,id,permalink_url,title,description,live_views`,
        { observe: 'response' }
      )
      .pipe(
        share(),
        map((response) => response.body)
      );
  }

  getPosts(pageId: string, pageAuthToken: string): Observable<FbPost[]> {
    return this.http
      .get<FbResponse<FbPost[]>>(
        `https://graph.facebook.com/v10.0/${pageId}/posts?access_token=${pageAuthToken}&limit=100`,
        {
          observe: 'response',
        }
      )
      .pipe(
        share(),
        map((response) => {
          let posts = response.body.data;
          return posts;
        })
      );
  }

  async getManyPosts(pageId: string, pages: number, pageAuthToken: string): Promise<FbPost[]> {
    let url = `https://graph.facebook.com/v10.0/${pageId}/posts?access_token=${pageAuthToken}&limit=100`;
    log.debug(`getManyPosts() URL: ${url}`);

    const posts: FbPost[] = [];

    let response = await this.http.get<FbResponse<FbPost[]>>(url, { observe: 'response' }).toPromise();
    for (let i = 1; i < pages; i++) {
      if (response.body.paging?.next) {
        posts.push(...response.body.data);
        response = await this.http
          .get<FbResponse<FbPost[]>>(response.body.paging.next, { observe: 'response' })
          .toPromise();
      } else {
        break;
      }
    }
    posts.push(...response.body.data);

    return posts;
  }

  getReactions(fullObjectId: string, limit: number, pageAuthToken: string): Observable<FbReaction[]> {
    let url = `https://graph.facebook.com/v10.0/${fullObjectId}/reactions?access_token=${pageAuthToken}&summary=true&limit=${limit}`;
    log.debug(`getReactions() URL: ${url}`);
    return this.http.get<FbResponse<FbReaction[]>>(url, { observe: 'response' }).pipe(
      share(),
      map((response) => {
        let reactions = response.body.data;
        return reactions;
      })
    );
  }

  getComments(postId: string, limit: number, pageAuthToken: string): Observable<FbComment[]> {
    let url = `https://graph.facebook.com/v10.0/${postId}/comments?access_token=${pageAuthToken}&summary=true&limit=${limit}&filter=stream&live_filter=no_filter&fields=id,message,from,message_tags,application,created_time`;
    log.debug(`getComments() URL: ${url}`);
    return this.http.get<FbResponse<FbComment[]>>(url, { observe: 'response' }).pipe(
      share(),
      map((response) => {
        let comments = response.body.data as FbComment[];
        return comments;
      })
    );
  }

  async getAllComments(postId: string, pageAuthToken: string): Promise<FbComment[]> {
    let url = `https://graph.facebook.com/v10.0/${postId}/comments?access_token=${pageAuthToken}&summary=true&limit=500&filter=stream&live_filter=no_filter&fields=id,message,from,message_tags,application,created_time`;
    log.debug(`getAllComments() URL: ${url}`);

    const comments: FbComment[] = [];

    let response = await this.http.get<FbResponse<FbComment[]>>(url, { observe: 'response' }).toPromise();
    while (response.body.paging?.next) {
      comments.push(...response.body.data);
      response = await this.http
        .get<FbResponse<FbComment[]>>(response.body.paging.next, { observe: 'response' })
        .toPromise();
    }
    comments.push(...response.body.data);

    return comments;
  }

  async getAllCommentsForPosts(postIds: string[], pageAuthToken: string): Promise<FbComment[][]> {
    log.debug(`getAllCommentsForPosts()`, postIds);

    const commentsPerPost: FbComment[][] = [];

    for (let i = 0; i < postIds.length; i++) {
      let comments: FbComment[] = [];
      let postId = postIds[i];
      let url = `https://graph.facebook.com/v10.0/${postId}/comments?access_token=${pageAuthToken}&summary=true&limit=500&filter=stream&live_filter=no_filter&fields=id,message,from,message_tags,application,created_time`;
      log.debug(`getAllCommentsForPosts() URL: ${url}`);
      let response = await this.http.get<FbResponse<FbComment[]>>(url, { observe: 'response' }).toPromise();
      while (response.body.paging?.next) {
        comments.push(...response.body.data);
        response = await this.http
          .get<FbResponse<FbComment[]>>(response.body.paging.next, { observe: 'response' })
          .toPromise();
      }
      comments.push(...response.body.data);
      commentsPerPost.push(comments);
    }

    return commentsPerPost;
  }

  replyToComment(comment: FbComment, pageAuthToken: string, message: string) {
    const contentId = comment.id;
    const reply = { message, id: contentId, pageAuthToken };
    return this.http
      .post<any>(`https://graph.facebook.com/v10.0/${contentId}/comments?access_token=${pageAuthToken}`, reply, {
        observe: 'response',
      })
      .pipe(
        share(),
        map((response) => response.body)
      );
  }

  postComment(pageId: string, hasTransitionedToNpe: boolean, video: FbVideo, pageAuthToken: string, message: string) {
    log.debug('postComment(): video:', video);
    const permalinkUrl = video.permalink_url;
    let objectId = `${video.from.id}_${video.video.id}`;
    log.debug('postComment(): objectId:', objectId);
    const reply = { message, id: objectId, pageAuthToken };
    return this.http
      .post<any>(`https://graph.facebook.com/v10.0/${objectId}/comments?access_token=${pageAuthToken}`, reply, {
        observe: 'response',
      })
      .pipe(
        share(),
        map((response) => response.body)
      );
  }

  deleteComment(comment: FbComment, pageAuthToken: string) {
    return this.http
      .delete<any>(`https://graph.facebook.com/v10.0/${comment.id}?access_token=${pageAuthToken}`, {
        observe: 'response',
      })
      .pipe(
        share(),
        map((response) => response.body)
      );
  }

  likeComment(comment: FbComment, pageAuthToken: string) {
    return this.http
      .put<any>(
        `https://graph.facebook.com/v10.0/${comment.id}/likes?access_token=${pageAuthToken}`,
        {},
        {
          observe: 'response',
        }
      )
      .pipe(
        share(),
        map((response) => response.body)
      );
  }
}

export class FbResponse<T> {
  public data: T;
  public paging: FbPaging;
}

export class FbPaging {
  public previous: string = '';
  public next: string = '';
  public cursors: FbCursors;
}

export class FbCursors {
  public after: string = '';
  public before: string = '';
}

export class FbPage {
  public id?: string;
  public name?: string;
  public access_token: string = '';
  public email: string = '';
  public tasks: string[] = [];
  public expires_in: number = 0;
}

export abstract class FbUserContent {
  abstract get fromId(): string;
  abstract get fromName(): string;
}

export class FbPost {
  public id?: string;
  public name?: string;
  public status_type?: string; // added_video, added_photos
  public permalink_url?: string;
  public message?: string;
  public created_time?: string;
}

export class FbVideo {
  public id?: string;
  public from?: FbUser;
  public video?: FbInnerVideo;
  public title?: string;
  public description?: string;
  public status?: string; // LIVE
  public permalink_url?: string;
  public live_views?: number;
}

export class FbInnerVideo {
  public id?: string;
}

export class FbReaction {
  public id?: string; // user id
  public name?: string;
  public type?: string; // enum {NONE, LIKE, LOVE, WOW, HAHA, SAD, ANGRY, THANKFUL, PRIDE, CARE}

  get fromId(): string {
    return this.id;
  }

  get fromName(): string {
    return this.name;
  }
}

export class FbComment extends FbUserContent {
  public from?: FbUser;
  public message?: string;
  public id?: string;
  public created_time: string;
  public application?: FbApplication;
  public message_tags: FbMessageTag[];
  public post_id?: string; // This is only available for webhooks (PAGEID_POSTID)
  public item?: string; // This is only available for webhooks (comment,reaction)
  public reaction_type?: string; // This is only available for webhooks (like,love)

  get fromId(): string {
    return this.from?.id;
  }

  get fromName(): string {
    return this.from?.name;
  }
}

export class FbUser {
  public name?: string;
  public id?: string;
}

export class FbUserResponse {
  public access_token: string = '';
  public name: string = '';
  public id: string = '';
  public email: string = '';
  public first_name: string = '';
  public last_name: string = '';
  public picture?: any;
}

export class FbPermission {
  public permission: string;
  public status: string; // granted, declined, expired
}

export class FbError {
  public message: string;
  public type: string;
  public code: number;
  public error_subcode: number;
  public error_user_title: string;
  public error_user_msg: string;
  public fbtrace_id: string;
}

export class FbApplication {
  public category?: string;
  public link?: string;
  public name?: string;
  public namespace?: string;
  public id?: string;
}

export class FbMessageTag {
  public id: string; // The ID of the tagged user or page
  public name: string; // The nameof the tagged user or page
  public offset: number; // The first character of the tag text (0-based)
  public length: number; // The length of the tag text
  public type: string; // "user" or "page"
}
