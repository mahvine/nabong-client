import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
})
export class DialogComponent implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData, private dialogRef: MatDialogRef<DialogComponent>) {
    this.dialogRef.disableClose = true;
  }

  ngOnInit(): void {}

  executePositiveAction() {
    this.dialogRef.close(DialogResult.POSITIVE);
  }

  executeNegativeAction() {
    this.dialogRef.close(DialogResult.NEGATIVE);
  }

  executeExtraAction() {
    this.dialogRef.close(DialogResult.EXTRA);
  }

  executeDestructiveAction() {
    this.dialogRef.close(DialogResult.DESTRUCTIVE);
  }
}

export class DialogData {
  public positiveActionText: string;
  public negativeActionText: string;
  public extraActionText: string;
  public destructiveActionText: string;

  public title: string;
  public message?: string;
  public html?: string;
}

export enum DialogResult {
  POSITIVE,
  NEGATIVE,
  DESTRUCTIVE,
  EXTRA,
}
