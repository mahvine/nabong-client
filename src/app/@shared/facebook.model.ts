export class VideoResponse {
  public data: Video[] = [];
}
export class PostResponse {
  public data: Post[] = [];
}

export class Video {
  public id?: string;
  public name?: string;
  public status?: string;
  public permalink_url?: string;
  public message?: string;
  public created_time?: string;
  public _post_id?: string; //this is an internal field to determine postId of video
}
export class Post {
  public id?: string;
  public message?: string;
  public status_type?: string;
  public permalink_url?: string;
}

export class StreamComment {
  public from?: User;
  public message?: string;
  public id?: string;
  public keyword?: string;
  public trimmedMessage?: string;
}

export class User {
  public name?: string;
  public id?: string;
}
