export class SocialUser {
  public access_token: string = '';
  public hashToken: string = '';
  public name: string = '';
  public id: string = '';
  public email: string = '';
  public first_name: string = '';
  public last_name: string = '';
  public picture?: any;
}
