import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransactionListComponent } from './transaction-list.component';

const routes: Routes = [{ path: '', component: TransactionListComponent, data: { title: 'Transactions' } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class TransactionRoutingModule {}
