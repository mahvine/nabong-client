import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TransactionModel } from '@app/@shared/services/nabong.model';
import { NotificationService } from '@app/@shared/services/notification.service';
import { NoxbotService } from '@app/@shared/services/noxbot.service';
import { CredentialsService } from '@app/auth';

@Component({
  selector: 'app-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.scss'],
})
export class TransactionListComponent implements OnInit {
  displayedColumns: string[] = ['createdAt', 'type', 'message', 'status', 'senderId', 'action'];
  isLoading = false;
  _transactions: TransactionModel[] = [];
  transactions = new MatTableDataSource(this._transactions);

  constructor(
    public dialog: MatDialog,
    public noxbotService: NoxbotService,
    public credentialsService: CredentialsService,
    private notificationService: NotificationService
  ) {}

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit(): void {
    console.log(`TransactionListComponent.ngOnInit()`);
    this.refreshData();
  }

  ngAfterViewInit() {
    console.log(`TransactionListComponent.ngAfterViewInit()`);
    this.transactions.sort = this.sort;
    this.transactions.sortingDataAccessor = (data, attribute) => data[attribute];
  }

  refreshData() {
    this.isLoading = true;
    this.noxbotService.getListOfTransactions().subscribe((response) => {
      this.isLoading = false;
      this._transactions = response.data;
      this.transactions.data = this._transactions;
    });
  }

  metadataKeys(metadata: any) {
    return Object.keys(metadata);
  }

  updateStatus(model: TransactionModel, status: string) {
    model.transactionStatus = status;
    this.noxbotService.updateStatus(model).subscribe((data) => {
      this.refreshData();
    });
  }
}
