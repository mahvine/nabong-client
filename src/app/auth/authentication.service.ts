import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { from, Observable, of } from 'rxjs';
import { Subject } from 'rxjs/internal/Subject';
import { map, catchError } from 'rxjs/operators';
import { CredentialsService } from './credentials.service';
import { FacebookService, LoginResponse, InitParams, LoginOptions } from 'ngx-facebook';

import { Logger } from '@app/@core';
import { SocialUser } from '@app/@shared/social-user.model';
const log = new Logger('AuthenticationService');

export interface LoginContext {
  username: string;
  password: string;
  remember?: boolean;
}

/**
 * Provides a base for authentication workflow.
 * The login/logout methods should be replaced with proper implementation.
 */
@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  private _onLogout = new Subject();

  private _protectedLocalStorageKeys: string[] = [];

  constructor(private credentialsService: CredentialsService, private http: HttpClient, private fb: FacebookService) {
    const initParams: InitParams = {
      appId: environment.fbAppId,
      xfbml: true,
      version: 'v10.0',
    };

    fb.init(initParams);
  }

  get onLogout() {
    return this._onLogout;
  }

  signInWithFB(): Observable<SocialUser> {
    let observable = new Subject<SocialUser>();

    log.debug('authenticationService.signInWithFB()');
    const fbLoginOptions = {
      scope: environment.scope,
    };
    this.fb
      .login(fbLoginOptions)
      .then((response: LoginResponse) => {
        log.debug(response);
        if (response.status === 'connected') {
          this.http
            .get<SocialUser>(`${environment.serverUrl}/api/custom?authToken=${response.authResponse.accessToken}`, {
              observe: 'response',
            })
            .pipe(
              // tslint:disable-next-line: no-shadowed-variable
              map((response) => {
                return response.body;
              }),
              catchError((error) => {
                log.error('authenticationService.signInWithFB() > error');
                log.error(error);
                observable.error(error);
                throw error;
              })
            )
            .subscribe((returnedUser) => {
              if (returnedUser) {
                log.debug('authenticationService.signInWithFB() > success');
                log.debug(returnedUser);
                this.credentialsService.setCredentials(returnedUser);
                observable.next(returnedUser);
              } else {
                log.debug('authenticationService.signInWithFB() > returnedUser null');
                observable.error('Facebook did not return a valid user.');
              }
            });
        } else {
          observable.error('Could not connect to Facebook. Please try again later.');
        }
      })
      .catch((error) => {
        log.error('authenticationService.signInWithFB() > catch any error');
        observable.error(error);
      });

    return observable;
  }

  /**
   * Logs out the user and clear credentials.
   * @return True if the user was logged out successfully.
   */
  logout(): Observable<boolean> {
    // Customize credentials invalidation here
    this.credentialsService.clearCredentials();

    // Clear session storage
    sessionStorage.clear();

    // Broadcast log out event
    this._onLogout.next();

    return of(true);
  }
}
