import { Injectable } from '@angular/core';
import { SocialUser } from '@app/@shared/social-user.model';
import { ManagedPageModel } from '@app/@shared/services/managedpage.model';
import { Subject } from 'rxjs';
import { Logger } from '@core/logger.service';
const log = new Logger('CredentialsService');

export interface Credentials {
  // Customize received credentials here
  username: string;
  token: string;
}

const credentialsKey = 'credentials';
const pageKey = 'page';

/**
 * Provides storage for authentication credentials.
 * The Credentials interface should be replaced with proper implementation.
 */
@Injectable({
  providedIn: 'root',
})
export class CredentialsService {
  private _credentials: SocialUser | null = null;
  private _activePage: ManagedPageModel | null;

  private _onSetPage = new Subject<ManagedPageModel>();

  constructor() {
    const savedCredentialsData = localStorage.getItem(credentialsKey);
    if (savedCredentialsData) {
      log.debug(`Saved credentials detected, loading...`);
      this.setCredentials(Object.assign(new SocialUser(), JSON.parse(savedCredentialsData)));
    } else {
      log.debug(`No saved credentials`);
    }

    const savedPageData = localStorage.getItem(pageKey);
    if (savedPageData) {
      log.debug(`Last active page detected, loading...`);
      this.setActivePage(Object.assign(new ManagedPageModel(), JSON.parse(savedPageData)));
    } else {
      log.debug(`No last active page detected`);
    }
  }

  /**
   * Checks is the user is authenticated.
   * @return True if the user is authenticated.
   */
  isAuthenticated(): boolean {
    return !!this.credentials;
  }

  /**
   * Gets the user credentials.
   * @return The user credentials or null if the user is not authenticated.
   */
  get credentials(): SocialUser | null {
    return this._credentials;
  }

  /**
   * Sets the user credentials.
   * The credentials may be persisted across sessions by setting the `remember` parameter to true.
   * Otherwise, the credentials are only persisted for the current session.
   * @param credentials The user credentials.
   * @param remember True to remember credentials across sessions.
   */
  setCredentials(credentials: SocialUser) {
    log.debug(`setCredentials()`);
    this._credentials = credentials;
    localStorage.setItem(credentialsKey, JSON.stringify(credentials));
  }

  clearCredentials() {
    log.debug(`clearCredentials()`);
    localStorage.removeItem(credentialsKey);
    localStorage.removeItem(pageKey);
    this._credentials = undefined;
    this._activePage = undefined;
  }

  get activePage(): ManagedPageModel | null {
    return this._activePage;
  }

  get onSetPage() {
    return this._onSetPage;
  }

  setActivePage(page?: ManagedPageModel) {
    log.debug(`setActivePage()`);
    this._activePage = page || null;

    if (page) {
      localStorage.setItem(pageKey, JSON.stringify(page));

      log.debug(`Page: ${this.activePage.name}`);
    } else {
      localStorage.removeItem(pageKey);
    }
    this._onSetPage.next(this._activePage);
  }
}
