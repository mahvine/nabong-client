import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { environment } from '@env/environment';
import { CoreModule, Logger } from '@core';
import { AuthenticationService } from './authentication.service';
import { SocialUser } from '@app/@shared/social-user.model';
import { MatDialog } from '@angular/material/dialog';
import { HttpErrorResponse } from '@angular/common/http';
import { DialogComponent, DialogData } from '@app/@shared/dialog/dialog.component';
import { FbError } from '@app/@shared/services/facebook-graph-api.service';

const log = new Logger('Login');

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  version: string | null = environment.version;
  error: string | undefined;
  loginForm!: FormGroup;
  isLoading = false;

  user: SocialUser;
  loggedIn: boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private coreModule: CoreModule,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.createForm();
  }

  ngOnDestroy() {}

  login() {
    log.info('loginComponent.login()');
    this.isLoading = true;

    this.authenticationService.signInWithFB().subscribe(
      (user) => {
        this.isLoading = false;
        if (user) {
          // Navigate
          this.router.navigate([this.route.snapshot.queryParams.redirect || '/'], { replaceUrl: true });
        }
      },
      (error: HttpErrorResponse | string) => {
        this.isLoading = false;
        log.error(`login() > signInWithFb() > error: `, error);
        if (error) {
          this.alertLogInError(error);
        }
      }
    );
  }

  alertLogInError(error: HttpErrorResponse | string) {
    let dialogData = new DialogData();
    dialogData.positiveActionText = 'OK';

    if (typeof error == 'string') {
      dialogData.message = error;
    } else {
      let fbError = error.error.data.error as FbError;
      let fbTraceId = fbError?.fbtrace_id;

      if (fbTraceId) {
        dialogData.message = `There was a problem logging in with your Facebook account, and we detected that it's probably a Facebook-related issue. If you can, please report this to the developers so we can see what we can do about this.
        
        Facebook error details:
        ${JSON.stringify(fbError, null, ' ')}`;
      } else {
        dialogData.message = `There was a problem logging in. Please try again later.
        
        Error details:
        ${JSON.stringify(error, null, ' ')}`;
      }
    }

    let dialogRef = this.dialog.open(DialogComponent, { data: dialogData });
  }

  private createForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      remember: true,
    });
  }
}
